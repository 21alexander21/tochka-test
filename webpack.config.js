const path = require("path");
const TSLintPlugin = require("tslint-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const devMode = process.env.NODE_ENV !== "production";

module.exports = {
  entry: path.join(__dirname, "/src/index.ts"),
  output: {
    filename: "app.js",
    path: path.join(__dirname, "dist")
  },
  module: {
    rules: [
      {
        test: /\.ts$/,
        loader: "ts-loader",
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        exclude: /node_modules/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              hmr: devMode,
              sourceMap: devMode
            }
          },
          {
            loader: "css-loader",
            options: {
              sourceMap: devMode
            }
          },
          {
            loader: "sass-loader",
            options: {
              sourceMap: devMode
            }
          }
        ]
      }
    ]
  },
  resolve: {
    extensions: [".ts", ".js"]
  },
  plugins: [
    new TSLintPlugin({
      files: ["./src/**/*.ts"]
    }),
    new MiniCssExtractPlugin({
      filename: devMode ? "[name].css" : "[name].[hash].css",
      chunkFilename: devMode ? "[id].css" : "[id].[hash].css"
    })
  ],
  devServer: {
    contentBase: path.join(__dirname, "public"),
    compress: true,
    port: 9000,
    clientLogLevel: "none",
    historyApiFallback: true
  }
};
