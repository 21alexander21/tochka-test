import RenderElement from "../../utils/RenderElement";
import BaseCard from "../BaseCard";
import NewsCard from "../NewsCard";
import "./styles.scss";

export type TSort = "type" | "date";

export interface IListProps<T> {
  children: T[];
  sort?: TSort;
  onClickAddCard?: EventListener;
}

function getComparator<T extends BaseCard>(sortType: TSort) {
  return (itemA: T, itemB: T) => {
    if (sortType === "date") {
      return itemA.dateOfCreate.valueOf() - itemB.dateOfCreate.valueOf();
    }

    if (sortType === "type") {
      const itemTypeA = itemA.constructor.name;
      const itemTypeB = itemB.constructor.name;

      if (itemTypeA > itemTypeB) {
        return 1;
      }

      if (itemTypeA < itemTypeB) {
        return -1;
      }

      return 0;
    }

    return 0;
  };
}

export default class List<T extends BaseCard> {
  public _: any;
  private set sortType(value: TSort) {
    this._sortType = value;
    this.sort();
  }

  private get sortType() {
    return this._sortType;
  }

  private _children: T[] = [];
  private _sortType: TSort = "date";

  private views: { [key: string]: HTMLElement };
  private props: IListProps<T>;

  public set children(value: T[]) {
    this._children = value;
    this.sort();
  }

  constructor(props: IListProps<T>) {
    this._children = props.children;
    this._sortType = props.sort || "date";

    this.props = props;
    this.views = this.renderViews();
  }

  public render = () => {
    const { container } = this.views;

    this.update();

    return container;
  }

  private update = () => {
    const { container } = this.views;

    if (Array.isArray(this._children)) {
      this._children.forEach((item) => {
        container.appendChild(item.renderPreview());
      });
    }
  }

  private sort = () => {
    this._children.sort(getComparator(this.sortType));
    this.update();
  }

  private handleClickAdd = (event: Event) => {
    if (this.props.onClickAddCard) {
      this.props.onClickAddCard(event);
    }
  }

  private handleClickSort = () => {
    if (this.sortType === "date") {
      this.sortType = "type";
    } else {
      this.sortType = "date";
    }
  }

  private renderControls = () => {
    const addButton = RenderElement("button", {
      classNames: ["btn", "btn--add-card"],
      children: "Add card",
    });

    const sortButton = RenderElement("button", {
      classNames: ["btn", "btn--sort"],
      children: "Sort",
    });

    addButton.addEventListener("click", this.handleClickAdd);
    sortButton.addEventListener("click", this.handleClickSort);

    return RenderElement("div", {
      classNames: "list__controls",
      children: [addButton, sortButton],
    });
  }

  private renderViews = () => {
    const controls = this.renderControls();
    const container = RenderElement("div", {
      classNames: "list__container",
    });
    const wrapper = RenderElement("div", {
      classNames: "list__wrapper",
    });

    container.append(controls);
    container.append(wrapper);

    return {
      controls,
      container,
      wrapper,
    };
  }
}
