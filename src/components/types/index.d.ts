export type TCurrency = "RUB" | "USD" | "EUR";

export type TTransactionType = "incoming" | "expense";
