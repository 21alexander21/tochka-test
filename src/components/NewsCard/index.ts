import RenderClickable from "../../utils/RenderClickable";
import RenderElement from "../../utils/RenderElement";
import Router from "../../utils/Router";
import BaseCard, { IBaseCardProps } from "../BaseCard/";
import "./styles.scss";

export interface INewsCardProps extends IBaseCardProps {
  title: string;
  content: string;
}

export default class NewsCard extends BaseCard {
  public title: string;
  public content: string;
  private _checked: boolean = false;

  private get checked() {
    return this._checked;
  }

  private set checked(checked) {
    this._checked = checked;
    this.updateViews();
  }

  constructor(props: INewsCardProps) {
    super(props);

    this.title = props.title;
    this.content = props.content;

    this.createViews();
    this.updateViews();
  }

  public renderPreviewContent = () => {
    return this.views.titleLink;
  }

  public renderFullContent = () => {
    return [this.views.title, this.views.content, this.views.button];
  }

  private handleClickCheck = () => {
    this.checked = true;
  }

  private createViews = () => {
    this.views.button = RenderClickable({
      children: "Got it!",
      onClick: this.handleClickCheck,
    });

    this.views.title = RenderElement("h1", {
      classNames: "news-card__title",
      children: this.title,
    });

    this.views.titleLink = RenderClickable({
      classNames: "news-card__title",
      use: "link",
      onClick: () => {
        window.history.pushState(
          { id: this.id },
          this.title,
          `/news/${this.id}`,
        );
        Router.routerForceUpdate();
      },
    });

    this.views.content = RenderElement("p", {
      classNames: "news-card__content",
      children: this.content,
    });
  }

  private updateViews = () => {
    if (this.views.button) {
      (this.views.button as HTMLButtonElement).disabled = this.checked;
    }

    if (this.views.title) {
      this.views.title.innerText = `${this.checked ? "✔️ " : ""}${this.title}`;
    }

    if (this.views.titleLink) {
      this.views.titleLink.innerText = `${this.checked ? "✔️ " : ""}${
        this.title
      }`;
    }
  }
}
