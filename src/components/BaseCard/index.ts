import createUid from "../../utils/CreateUid";
import RenderElement from "../../utils/RenderElement";
import "./styles.scss";

export interface IBaseCardProps extends Object {
  dateOfCreate?: Date;

  contentClassName?: string;
  containerClassName?: string;
}

export default abstract class BaseCard {
  get id() {
    return this._id;
  }

  get dateOfCreate() {
    return this._dateOfCreate;
  }

  public views: { [key: string]: HTMLElement } = {};

  private _id: string;
  private _dateOfCreate: Date;

  private contentClassName?: string;

  private containerNode: HTMLElement;
  private contentNode = RenderElement("div");

  constructor(props: IBaseCardProps) {
    this._id = createUid();
    this._dateOfCreate = props.dateOfCreate || new Date();

    this.contentClassName = props.contentClassName;

    this.containerNode = RenderElement("div", {
      classNames: ["card"],
      children: this.renderDateTime(),
    });

    if (props.containerClassName) {
      this.containerNode.classList.add(props.containerClassName);
    }

    this.drawContainer();
  }

  public renderFullContent = (): HTMLElement | HTMLElement[] => {
    return RenderElement("div", {
      classNames: this.contentClassName,
      children: `I'm BaseCard and my id is ${this.id}`,
    });
  }

  public renderPreviewContent = (): HTMLElement | HTMLElement[] => {
    return RenderElement("div", {
      classNames: this.contentClassName,
      children: `I'm BaseCard and my id is ${this.id}`,
    });
  }

  public render = () => this.renderContainer("full");

  public renderPreview = () => this.renderContainer("preview");

  private renderContainer = (mode: "full" | "preview" = "preview") => {
    const content =
      mode === "preview"
        ? this.renderPreviewContent()
        : this.renderFullContent();

    this.contentNode.innerHTML = "";
    if (Array.isArray(content)) {
      content.forEach((item, index) => {
        if (item instanceof HTMLElement) {
          this.contentNode.appendChild(item);
        } else {
          // tslint:disable-next-line:no-console
          console.warn(
            `Element ${item} (index: ${index}) is not a valid HTML node`,
          );
        }
      });
    } else {
      this.contentNode.appendChild(content);
    }

    this.containerNode.append(this.contentNode);
    return this.containerNode;
  }

  private drawContainer = () => {
    this.containerNode.classList.add("card");
    this.animateContainer();
  }

  private animateContainer = () => {
    this.containerNode.classList.add("card--fade");

    setTimeout(() => {
      this.containerNode.classList.remove("card--fade");
    }, 0);
  }

  private renderDateTime = () =>
    RenderElement("div", {
      classNames: "card__date-time",
      children: `${this.dateOfCreate.toLocaleDateString(
        "ru-RU",
      )} - ${this.dateOfCreate.toLocaleTimeString("ru-RU")}`,
    })
}
