import RenderClickable from "../../utils/RenderClickable";
import RenderElement from "../../utils/RenderElement";
import Router from "../../utils/Router";
import BaseCard, { IBaseCardProps } from "../BaseCard/";
import { TCurrency, TTransactionType } from "../types";
import renderSum from "./renderSum";
import "./styles.scss";

export interface ITransactionProps extends IBaseCardProps {
  sum: number;
  currency: TCurrency;
  use: string;
  description: string;
  transactionType: TTransactionType;
  onDelete: (id: string) => void;
}

export default class Transaction extends BaseCard {
  private props: ITransactionProps;

  constructor(props: ITransactionProps) {
    super(props);

    this.props = props;

    this.createViews();
  }

  public renderPreviewContent = () => {
    const { sum, content, link } = this.views;
    return [sum, content, link];
  }

  public renderFullContent = () => {
    const { sum, content, description, deleteButton } = this.views;
    return [sum, content, description, deleteButton];
  }

  private createViews = () => {
    this.views.sum = renderSum(
      this.props.transactionType === "expense"
        ? -1 * this.props.sum
        : this.props.sum,
      this.props.currency,
    );

    this.views.link = RenderClickable({
      children: "Подробнее...",
      onClick: (event) => {
        event.preventDefault();
        window.history.pushState({}, `Транзакция`, `/transaction/${this.id}`);
        Router.routerForceUpdate();
      },
    });
    this.views.deleteButton = RenderClickable({
      children: "✖️ Удалить",
      onClick: () => this.props.onDelete(this.id),
    });
    this.views.description = RenderElement("p", {
      classNames: "transaction__description",
      children: this.props.description,
    });
    this.views.content = RenderElement("p", {
      classNames: "transaction__content",
      children:
        this.props.transactionType === "incoming"
          ? `От кого: ${this.props.use}`
          : `Кому: ${this.props.use}`,
    });
  }
}
