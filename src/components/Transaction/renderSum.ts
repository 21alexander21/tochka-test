import FormatSum from "../../utils/FormatSum";
import RenderElement from "../../utils/RenderElement";
import { TCurrency } from "../types";

export default (sum: number, currency: TCurrency) => {
  const isNegative = sum < 0;
  const formattedSum = FormatSum(sum, currency);

  const classNames = ["sum", isNegative ? "sum--negative" : "sum--positive"];

  return RenderElement("span", {
    classNames,
    children: formattedSum,
  });
};
