import { App } from "./app";
import "./styles.scss";

function InitApp() {
  const root = document.getElementById("root");
  const app = new App();

  if (root) {
    let renderedApp = root.appendChild(app.render());

    document.addEventListener("update", () => {
      root.removeChild(renderedApp);
      renderedApp = root.appendChild(app.render());
    });
  }
}

InitApp();
