import BaseCard from "./components/BaseCard";
import List from "./components/List";
import NewsCard from "./components/NewsCard";
import Transaction from "./components/Transaction";
import RenderElement from "./utils/RenderElement";
import Router from "./utils/Router";

export class App {
  private router: Router;

  private cards: Array<NewsCard | Transaction>;
  private counter = 1;
  private list: List<NewsCard | Transaction>;
  private container = RenderElement("div", {
    classNames: "app",
  });

  constructor() {
    this.cards = [
      new NewsCard({
        title: "Second card",
        content: "It's content of second card",
      }),
      new Transaction({
        sum: 10000,
        currency: "RUB",
        use: "Мне",
        description: "За работу",
        transactionType: "incoming",
        onDelete: this.handleDeleteTransition,
      }),
      new Transaction({
        sum: 5050,
        currency: "RUB",
        use: "Не мне",
        description: "За товары",
        transactionType: "expense",
        onDelete: this.handleDeleteTransition,
      }),
    ];

    this.list = new List({
      children: this.cards,
      onClickAddCard: this.addCard,
    });

    this.router = this.configureRouter();
  }

  public render = () => {
    if (
      this.router.currentRouteData &&
      this.router.currentRouteData.component
    ) {
      this.container.innerHTML = "";
      this.container.appendChild(this.router.currentRouteData.component());
    }

    return this.container;
  }

  private addCard = () => {
    const number = this.counter++;
    const card = new NewsCard({
      title: `Card #${number}`,
      content: `Card #${number}`,
    });
    this.cards.push(card);

    this.list.children = this.cards;
  }

  private getNews = () => {
    if (
      this.router.currentRouteData &&
      this.router.currentRouteData.variables.newsId
    ) {
      const { newsId } = this.router.currentRouteData.variables;
      const card = this.cards.find(
        (item) => item instanceof NewsCard && item.id.toString() === newsId,
      );

      if (card) {
        return card.render();
      }

      return Router.route404();
    }
    return Router.route404();
  }

  private getTransaction = () => {
    if (
      this.router.currentRouteData &&
      this.router.currentRouteData.variables.transactionId
    ) {
      const { transactionId } = this.router.currentRouteData.variables;
      const card = this.cards.find(
        (item) =>
          item instanceof Transaction && item.id.toString() === transactionId,
      );

      if (card) {
        return card.render();
      }

      return Router.route404();
    }
    return Router.route404();
  }

  private configureRouter = () => {
    const page404 = document.createElement("h1");
    page404.innerText = "404";
    return new Router({
      routes: {
        "/": this.list.render,
        "/news/::newsId": this.getNews,
        "/transaction/::transactionId": this.getTransaction,
      },
      onRouteChange: this.render,
    });
  }

  private handleDeleteTransition = (id: string) => {
    this.cards.filter((item) => {
      if (item instanceof Transaction && id === item.id) {
        return false;
      }

      return true;
    });

    window.history.pushState({}, `Список`, `/`);
    Router.routerForceUpdate();
  }
}
