import { TCurrency } from "../../components/types";

export default function(sum: number, currency: TCurrency) {
  return sum.toLocaleString("ru-RU", { style: "currency", currency });
}
