import RenderElement from "../RenderElement";

interface IRouterProps {
  routes: {
    [key: string]: () => HTMLElement;
  };
  onRouteChange: () => void;
}

interface IRouteData {
  route: string;
  path: string;
  component?: () => HTMLElement;
  variables: {
    [key: string]: string;
  };
}

const routerUpdateEventType = "RouterForceUpdate";

export default class Router {
  public static route404 = () => {
    const page404 = RenderElement("div", {
      children: RenderElement("h1", { children: "404" }),
    });
    return page404;
  }
  public static routerForceUpdate = () =>
    document.dispatchEvent(new Event(routerUpdateEventType))
  private static variableToken = "::";
  private static getVariablePart = (routePart: string) => {
    if (
      routePart.startsWith(Router.variableToken) &&
      routePart.length > Router.variableToken.length
    ) {
      return {
        variableName: routePart.replace(Router.variableToken, ""),
      };
    }

    return null;
  }
  private static matchRoute = (route: string, path: string) => {
    const routeParts = route.split("/");
    const pathParts = path.split("/");

    if (routeParts.length !== pathParts.length) {
      return null;
    }

    const routeData: IRouteData = {
      route,
      path,
      variables: {},
    };

    for (let i = 0; i < route.length; i += 1) {
      const routePart = routeParts[i];
      const pathPart = pathParts[i];

      if (routePart === pathPart) {
        continue;
      }

      const variablePart = Router.getVariablePart(routePart);

      if (variablePart) {
        routeData.variables[variablePart.variableName] = pathPart;
        continue;
      }

      return null;
    }

    return routeData;
  }

  public currentRouteData: IRouteData | null;

  private props: IRouterProps;

  constructor(props: IRouterProps) {
    this.props = props;
    this.currentRouteData = this.getCurrentRoute();

    this.addListeners();
  }

  private getCurrentRoute = () => {
    const { routes } = this.props;
    let currentRouteData: IRouteData | null = null;

    Object.keys(routes).forEach((key) => {
      const overlap = Router.matchRoute(key, document.location.pathname);
      if (overlap) {
        currentRouteData = {
          ...overlap,
          component: routes[overlap.route],
        };
      }
    });

    return currentRouteData;
  }

  private update = () => {
    this.currentRouteData = this.getCurrentRoute();
    this.props.onRouteChange();
  }

  private addListeners = () => {
    window.history.replaceState(
      { id: null },
      document.title,
      document.location.href,
    );

    window.addEventListener("popstate", this.update);
    document.addEventListener(routerUpdateEventType, this.update);
  }
}
