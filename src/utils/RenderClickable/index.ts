import RenderElement, { IRenderAttrs } from "../RenderElement";
import "./styles.scss";

interface IClicableProps extends IRenderAttrs {
  onClick: (event: Event) => void;
  use?: "link" | "button";
}

export default function(props: IClicableProps) {
  const { onClick, use = "button", ...rest } = props;
  const classNames = [use === "button" ? "button" : "link"];

  if (Array.isArray(rest.classNames)) {
    rest.classNames.forEach((item) => {
      if (typeof item === "string") {
        classNames.push(item);
      }
    });
  }

  if (typeof rest.classNames === "string") {
    classNames.push(rest.classNames);
  }

  const node = RenderElement("button", {
    ...rest,
    classNames,
  });

  node.addEventListener("click", onClick);

  return node;
}
