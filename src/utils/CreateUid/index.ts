/**
 * This function just was googled :)
 */
export default function createUid() {
  // tslint:disable-next-line:no-bitwise
  let firstPart: number | string = (Math.random() * 46656) | 0;
  // tslint:disable-next-line:no-bitwise
  let secondPart: number | string = (Math.random() * 46656) | 0;

  firstPart = `000${firstPart.toString(36)}`.slice(-3);
  secondPart = `000${secondPart.toString(36)}`.slice(-3);

  return firstPart + secondPart;
}
