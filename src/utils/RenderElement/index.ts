export interface IRenderAttrs {
  classNames?: string | string[];
  children?: string | HTMLElement | Array<string | HTMLElement>;
}

export default function<K extends keyof HTMLElementTagNameMap>(
  tag: K,
  attrs: IRenderAttrs = {},
) {
  const node = document.createElement(tag);
  const { classNames, children } = attrs;

  if (classNames) {
    if (Array.isArray(classNames)) {
      node.classList.add(...classNames);
    } else {
      node.classList.add(classNames);
    }
  }

  if (children) {
    if (Array.isArray(children)) {
      children.forEach((item) => node.append(item));
    } else {
      node.append(children);
    }
  }

  return node;
}
